package api

import (
	"github.com/fsouza/go-dockerclient"
	"github.com/kataras/iris"
	"strconv"
)

type ListContainers struct {
	Container string
	ListOpts  docker.ListContainersOptions
}

func (c *ListContainers) AddContext (ctx *iris.Context) *ListContainers {
	var err error

	qAll 		:= ctx.URLParam("all")
	qSize 		:= ctx.URLParam("size")
	qLimit 		:= ctx.URLParam("limit")
	qSince 		:= ctx.URLParam("since")
	qBefore 	:= ctx.URLParam("before")
	qFilters 	:= ctx.URLParam("filters")

	if qAll != "" {
		c.ListOpts.All, err 	= strconv.ParseBool(qAll)
		if err != nil {

		}
	}
	if qSize != "" {
		c.ListOpts.Size, err 	= strconv.ParseBool(qSize)
		if err != nil {

		}
	}
	if qLimit != "" {
		c.ListOpts.Limit, err 	= strconv.Atoi(qLimit)
	}
	if qSince != "" {
		c.ListOpts.Since 	= strconv.Quote(qSince)
	}
	if qBefore != "" {
		c.ListOpts.Before 	= strconv.Quote(qBefore)
	}
	if qFilters != "" {
		c.ListOpts.Filters, err = MapFilters(qFilters)
		if err != nil {

		}
	}


	return c
}




