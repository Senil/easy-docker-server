package api

import (
	"github.com/kataras/iris"
)

type ContainerAction struct {
	Container string
	Timeout uint
}

func (c ContainerAction) AddContext (ctx *iris.Context) ContainerAction {
	c.Container = ctx.Param("cid")
	if c.Container == "" {

	}

	t, err 	:= ctx.URLParamInt("t")

	if err != nil {
		t = 5
	}
	c.Timeout = uint(t)

	return c
}


