package api

import (
	"github.com/Sirupsen/logrus"
)

var log = logrus.New()
//
//@todo add the formatter as option to the cli
//

func init() {
	log.Formatter = new(logrus.JSONFormatter)
	//log.Formatter = new(logrus.TextFormatter) // default
	log.Level = logrus.DebugLevel
}



