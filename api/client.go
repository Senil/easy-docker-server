package api

import (
	"flag"
	"github.com/fsouza/go-dockerclient"
)

var (
	endpoint 	string
	cert 		string
	key 		string
	ca		string
	apiVersion	string
	certPEMBlock 	[]byte
	keyPEMBlock	[]byte
	caPEMCert	[]byte

)

type DockerClient struct {

	Connect *docker.Client
}

func init() {
	initCliArgs()
}

func initCliArgs() {
	flag.StringVar(&endpoint, "endpoint", "", "server endpoint")
	flag.StringVar(&cert, "cert", "", "certificate")
	flag.StringVar(&key, "key", "", "key")
	flag.StringVar(&ca, "ca", "", "ca")
	flag.StringVar(&apiVersion, "apiVersion", "", "apiVersion")
	flag.Parse()
}

func (dc *DockerClient) Dial() *DockerClient {

	if endpoint == "" {
		panic("The endpoint muest be set")
	}
	// setup tls client
	if endpoint != "" && cert!= "" && key != "" && ca != "" {
		dc.Connect, _ = docker.NewTLSClient(endpoint, cert, key, ca)

		return dc
	}
	// setup versioned client
	if endpoint != "" && apiVersion!= "" {
		dc.Connect, _ = docker.NewVersionedClient(endpoint, apiVersion)

		return dc
	}

	dc.Connect, _ = docker.NewClient(endpoint)

	return dc
}
