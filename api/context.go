package api

type EasyDockerCtx struct {
	DockerClient
	Options
	ListContainers
	LogContainer
	ContainerAction
}