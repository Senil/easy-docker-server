package api

import (
	"encoding/json"
)

func MapFilters(query string) (map[string][]string, error){
	var filters map[string][]string

	err := json.Unmarshal([]byte(query), &filters)

	if err != nil {
		log.Warning(err)
	}

	return filters, err
}