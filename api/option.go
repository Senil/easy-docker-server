package api

import (
	"github.com/fsouza/go-dockerclient"
)

type Options struct {
	HostConfig 	*docker.HostConfig
	KillContainer 	*docker.KillContainerOptions

}


func (o Options)  GetOptions() Options {
	o.HostConfig 	= new(docker.HostConfig)
	o.KillContainer = new(docker.KillContainerOptions)

	return o
}

