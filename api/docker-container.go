package api

import (
	"github.com/kataras/iris"
)

const(
	RouteIndex 	= "/docker"
	RouteContainer  = "/containers"
)

var(
	container = iris.Party(RouteIndex)

)

// Run the Container Controller
func RunContainerCtrl () {
	easyDocker 	:= EasyDockerCtx{}
	client 		:= easyDocker.Dial()

	// get a list of containers
	container.Get("/containers", func(ctx *iris.Context) {
		options := easyDocker.ListContainers.AddContext(ctx)
		containers, _ := client.Connect.ListContainers(options.ListOpts)
		ctx.JSON(iris.StatusOK, containers)
	})
	// inspect a single container
	container.Get(RouteContainer + "/:cid", func(ctx *iris.Context) {
		cid := ctx.Param("cid")
		container, _ := client.Connect.InspectContainer(cid)
		ctx.JSON(iris.StatusOK, container)
	})
	// start a single container
	container.Get("/containers/:cid/start", func(ctx *iris.Context) {
		log.Info("starting Container")
		options := easyDocker.ContainerAction.AddContext(ctx)
		err  := client.Connect.StartContainer(options.Container, easyDocker.HostConfig)

		if err != nil {
			ctx.JSON(iris.StatusBadRequest, err)
		} else {
			log.Info("starting Container done")
			ctx.JSON(iris.StatusNoContent, nil)
		}
	})
	// stop a single container
	container.Get(RouteContainer + "/:cid/stop", func(ctx *iris.Context) {
		var err error
		options := easyDocker.ContainerAction.AddContext(ctx)
		err  = client.Connect.StopContainer(options.Container, options.Timeout)
		if err != nil {
			ctx.JSON(iris.StatusBadRequest, err)
		} else {
			ctx.JSON(iris.StatusNoContent, nil)
		}
	})
	// restart a single container
	container.Get(RouteContainer + "/:cid/restart", func(ctx *iris.Context) {
		var err error
		options := easyDocker.ContainerAction.AddContext(ctx)
		err  = client.Connect.RestartContainer(options.Container, options.Timeout)
		if err != nil {
			ctx.JSON(iris.StatusBadRequest, err)
		} else {
			ctx.JSON(iris.StatusNoContent, nil)
		}
	})
	// kill a single container
	container.Get(RouteContainer + "/:cid/kill", func(ctx *iris.Context) {
		options := easyDocker.ContainerAction.AddContext(ctx)
		easyDocker.KillContainer.ID = options.Container
		err  := client.Connect.KillContainer(*easyDocker.KillContainer)
		if err != nil {
			ctx.JSON(iris.StatusBadRequest, err)
		} else {
			ctx.JSON(iris.StatusNoContent, nil)
		}
	})
	// List processes running inside a container
	//@see https://goo.gl/Xppi5l
	container.Get(RouteContainer + "/:cid/top", func(ctx *iris.Context) {
		options := easyDocker.ContainerAction.AddContext(ctx)
		top, err  := client.Connect.TopContainer(options.Container, "")
		if err != nil {
			ctx.JSON(iris.StatusBadRequest, err)
		} else {
			ctx.JSON(iris.StatusOK, top)
		}
	})

	container.Get(RouteContainer + "/:cid/logs", func(ctx *iris.Context) {
		options := easyDocker.LogContainer.AddContext(ctx)
		err := client.Connect.Logs(options.LogsOpts)

		if err != nil {
			ctx.JSON(iris.StatusBadRequest, err)
		}
	})
}