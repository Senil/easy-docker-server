package api

import (
	"github.com/fsouza/go-dockerclient"
	"github.com/kataras/iris"
	//"strconv"

	"strconv"
)

type LogContainer struct {
	Container string
	LogsOpts docker.LogsOptions
}

func (c *LogContainer) AddContext (ctx *iris.Context) *LogContainer {

	var err error

	// params for the ListContainer Options
	container 	:= ctx.Param("cid")
	// params for the ListContainer Options
	qSince		:= ctx.URLParam("since")
	qFollow		:= ctx.URLParam("follow")
	qStdout		:= ctx.URLParam("stdout")
	qStderr		:= ctx.URLParam("stderr")
	qTimestamps	:= ctx.URLParam("timestamps")
	qTail		:= ctx.URLParam("tail")

	if qFollow != "" {
		c.LogsOpts.Follow, err 	= strconv.ParseBool(qFollow)
		if err != nil {

		}
	}
	if qStdout != "" {
		c.LogsOpts.Stdout, err 	= strconv.ParseBool(qStdout)
		if err != nil {

		}
	}
	if qStderr != "" {
		c.LogsOpts.Stderr, err 	= strconv.ParseBool(qStderr)
		if err != nil {

		}
	}
	if qTimestamps != "" {
		c.LogsOpts.Timestamps, err 	= strconv.ParseBool(qTimestamps)
		if err != nil {

		}
	}
	if qSince != "" {
		c.LogsOpts.Since, err 	= strconv.ParseInt(qSince, 10, 0)
		if err != nil {

		}
	}
	if qTail != "" {
		c.LogsOpts.Tail 	= strconv.Quote(qTail)
		if err != nil {

		}
	}
	if container != "" {
		c.LogsOpts.Container = container
	}

	return c
}
