package main

import (
	"github.com/kataras/iris"
	"bitbucket.org/Senil/easy-docker-server/api"
)

func main() {
	initRoutes()
	iris.Listen(":8085")
}

func initRoutes() {
	api.RunImageCtrl()
	api.RunContainerCtrl()
}
